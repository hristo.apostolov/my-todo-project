(() => {

    render(homePage());
    handleMenu();

    checkUsersRepository();

})();

async function checkUsersRepository() {
    if (await UsersRepository.count() == 0) {
        seedUsers();
    }
}

async function seedUsers() {
    const initialUser = new User('admin', 'adminpass', 'Administrator', 'Administrator', true);
    await UsersRepository.addUser(initialUser);
}

function render(innerHtml) {
    let contentDiv = document.getElementById('content');
    contentDiv.innerHTML = innerHtml;
}

async function handleMenu() {

    let loggedUser = await AuthenticationService.getLoggedUser();
    if(loggedUser != null) {
        let isAdmin = loggedUser._isAdmin;

        if(isAdmin==false){
            handleMenuUser();
        const loggedUserName = document.getElementById('loggedUserName');
        const userName = document.createElement('p');
        userName.innerHTML = loggedUser._username;
        loggedUserName.appendChild(userName);
            
        }if(isAdmin) {
            handleMenuAdmin();
            const loggedAdminName = document.getElementById('loggedUserName');
            const userName = document.createElement('p');
            userName.innerHTML = loggedUser._username;
            loggedAdminName.appendChild(userName);
        }

    } else {
        handleMenuGuest();
        const loggedUserName = document.getElementById('loggedUserName');
       loggedUserName.textContent="You are guest, please log in"
     
    }


}


function handleMenuUser() {
    menuDisplayFunction('none', '', 'none');
}

function handleMenuGuest() {
    menuDisplayFunction('', 'none', 'none');
}

function handleMenuAdmin() {
    menuDisplayFunction('none', '', '');
}

function menuDisplayFunction(displayGuest, displayUser, displayAdmin) {
    document.querySelectorAll("*[data-show='guest']").forEach(
        item => item.style.display = displayGuest);
   
    document.querySelectorAll("*[data-show='user']").forEach(
        item => item.style.display = displayUser);

    document.querySelectorAll("*[data-show='admin']").forEach(
        item => item.style.display = displayAdmin);
    
}