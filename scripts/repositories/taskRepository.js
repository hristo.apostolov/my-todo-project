class TaskRepository {

    static tasks = 'tasks';


    static async getAll() {

        return JSON.parse(await window.localStorage.getItem(this.tasks));
    }   
 

    static async count() {
        const items = await this.getAll();
        return items == null ? 0 : items.length;
    }

    static async getById(id) {

        const items = await this.getAll();
        for (let i = 0; i < items.length; i++) {
            const currentItem = items[i];
            if (currentItem._id == id) {

                return currentItem;
            }
        }
    }


    static async getByUsernameAndPassword(username, password) {
        const items = await this.getAll();
        for (let i = 0; i < items.length; i++) {
            const currentItem = items[i];
            if (currentItem._username == username && currentItem._password == password) {

                return currentItem;
            }
        }

        return null;
    }

    static getNextId(items) {

        if (items == undefined | null) {
            return 1;
        }

        return items[items.length - 1]._id + 1;
    }

    static async addTask(item) {
        let items = await this.getAll();
        const id = this.getNextId(items);
        item._id = id;
        if (items == null)
            items = [];

        items.push(item);
        await window.localStorage.setItem(this.tasks, JSON.stringify(items));
    }

    static async editTask(id, item) {
        const items = await this.getAll();
        for (let i = 0; i < items.length; i++) {
            const currentItem = items[i];
            if (currentItem._id == id) {
                currentItem._title = item.title;
                currentItem._description = item.description;
                currentItem._isComplete = item.isComplete;
            }
        }

        await window.localStorage.setItem(this.tasks, JSON.stringify(items));
    }

    static async deleteTask(id) {

        const items = await this.getAll();
        for (let i = 0; i < items.length; i++) {
            const currentItem = items[i];
            if (currentItem._id == id) {
                items.splice(i, 1);
            }
        }

        await window.localStorage.setItem(this.tasks, JSON.stringify(items));
    }
}

