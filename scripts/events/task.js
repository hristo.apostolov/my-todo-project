
async function tasksLink_Click() {

    await render(taskPage());
    const tasksTable = document.getElementById('tasksTable');

    const items = await TaskRepository.getAll();
    if (items == null)
        return;

    for (let i = 0; i < items.length; i++) {
        const currentItem = items[i];

        const tr = document.createElement('TR');

        const idTd = document.createElement('TD');
        idTd.innerHTML = currentItem._id;

        const assignedTd = document.createElement('TD');
  
        const lists = await ListRepository.getAll();

          for (let i = 0; i < lists.length; i++) {
            const list = lists[i];
            console.log(list._id);
            if (list._id == currentItem._assignedList) {
            assignedTd.innerHTML = list._title;
            }
        }

        const titleTd = document.createElement('TD');
        titleTd.innerHTML = currentItem._title;

        const descriptionTd = document.createElement('TD');
        descriptionTd.innerHTML = currentItem._description;

        const completedTd = document.createElement('TD');
        completedTd.innerHTML = currentItem._isComplete;
        console.log(currentItem._isComplete);
       
        const createdTd = document.createElement('TD');
        createdTd.innerHTML = currentItem._created;

        const createdbyTd = document.createElement('TD');
        createdbyTd.innerHTML = " ";

        const updatedTd = document.createElement('TD');
        updatedTd.innerHTML = currentItem._updated;

        const editTd = document.createElement('TD');
        const editButton = document.createElement('BUTTON');
        editButton.innerHTML = 'EDIT';
        editButton.addEventListener('click', () => tasksEditButton_Click(currentItem._id));
        editTd.appendChild(editButton);

        const deleteTd = document.createElement('TD');
        const deleteButton = document.createElement('BUTTON');
        deleteButton.innerHTML = 'DELETE';
        deleteButton.addEventListener('click', () => tasksDeleteButton_Click(currentItem._id));
        deleteTd.appendChild(deleteButton);

        tr.appendChild(idTd);
        tr.appendChild(assignedTd);
        tr.appendChild(titleTd);
        tr.appendChild(descriptionTd);
        tr.appendChild(completedTd);
        tr.appendChild(createdTd);
        tr.appendChild(createdbyTd);
        tr.appendChild(updatedTd);
        tr.appendChild(editTd);
        tr.appendChild(deleteTd);

        tasksTable.appendChild(tr);
        //console.log(currentItem._id)
    }
}


async function tasksEditLink_Click() {
    await render(taskEditPage());
}

async function tasksEditButton_Click(id) {

    await tasksEditLink_Click();
    const item = await TaskRepository.getById(id);

    document.getElementById('id').value = item._id;
    document.getElementById('title').value = item._title;
    document.getElementById('description').value = item._description;
    document.getElementById('isComplete').checked = item._isComplete;
    document.getElementById('assignedList').checked = item._assignedList;

}



async function tasksEditForm_Submit() {
    const id = document.getElementById('id').value;
    const title = document.getElementById('title').value;
    const description = document.getElementById('description').value;
    const isCompleted = (document.getElementById('isComplete').checked ? "yes" : "no");
    const assignedList = document.getElementById('assignedList').value;

    const item = new Task(title);
    item._description=description;
    item._isComplete=isCompleted;
    item._assignedList=assignedList;

    if (id == "") {
        await TaskRepository.addTask(item);
    } else {
        await TaskRepository.editTask(id, item);
       
    }

    await tasksLink_Click();
}

async function tasksDeleteButton_Click(id) {

    await TaskRepository.deleteTask(id);
    await tasksLink_Click();
}
