
async function todosLink_Click() {


    let loggedUser = await AuthenticationService.getLoggedUser();
    if (loggedUser != null) {
        let isAdmin = loggedUser._isAdmin;

        if (isAdmin == false) {
            await render(listPage());
            const listTable = document.getElementById('listTable');

            const items = await ListRepository.getAll();

            if (items == null)
                return;

            for (let i = 0; i < items.length; i++) {
                const currentItem = items[i];
                if (currentItem._assignedUser == loggedUser._id) {
                    console.log(loggedUser._id);
                    const tr = document.createElement('TR');

                    const idTd = document.createElement('TD');
                    idTd.innerHTML = currentItem._id;

                    const titleTd = document.createElement('TD');
                    titleTd.innerHTML = currentItem._title;

                    const createdTd = document.createElement('TD');
                    createdTd.innerHTML = currentItem._created.toLo;

                    const updatedTd = document.createElement('TD');
                    updatedTd.innerHTML = currentItem._updated;

                    const assignedUserTd = document.createElement('TD');

                    const users = await UsersRepository.getAll();

                    for (let i = 0; i < users.length; i++) {
                        const user = users[i];
                        //console.log(user._id);
                        if (user._id == currentItem._assignedUser) {
                            assignedUserTd.innerHTML = user._username;
                        }
                    }

                    const modifierTd = document.createElement('TD');
                    modifierTd.innerHTML = " ";

                    const editTd = document.createElement('TD');
                    const editButton = document.createElement('BUTTON');
                    editButton.innerHTML = 'EDIT';
                    editButton.addEventListener('click', () => listEditButton_Click(currentItem._id));
                    editTd.appendChild(editButton);

                    const deleteTd = document.createElement('TD');
                    const deleteButton = document.createElement('BUTTON');
                    deleteButton.innerHTML = 'DELETE';
                    deleteButton.addEventListener('click', () => listDeleteButton_Click(currentItem._id));
                    deleteTd.appendChild(deleteButton);

                    tr.appendChild(idTd);
                    tr.appendChild(titleTd);
                    tr.appendChild(createdTd);
                    tr.appendChild(updatedTd);
                    tr.appendChild(assignedUserTd);
                    tr.appendChild(modifierTd);
                    tr.appendChild(editTd);
                    tr.appendChild(deleteTd);

                    listTable.appendChild(tr);
                    //console.log(currentItem._id)
                }
            }

        } if (isAdmin) {
            await render(listPage());
            const listTable = document.getElementById('listTable');

            const items = await ListRepository.getAll();
            if (items == null)
                return;

            for (let i = 0; i < items.length; i++) {
                const currentItem = items[i];
                const tr = document.createElement('TR');

                const idTd = document.createElement('TD');
                idTd.innerHTML = currentItem._id;

                const titleTd = document.createElement('TD');
                titleTd.innerHTML = currentItem._title;

                const createdTd = document.createElement('TD');
                createdTd.innerHTML = currentItem._created;

                const updatedTd = document.createElement('TD');
                updatedTd.innerHTML = currentItem._updated;

                const assignedUserTd = document.createElement('TD');

                const users = await UsersRepository.getAll();

                for (let i = 0; i < users.length; i++) {
                    const user = users[i];
                    console.log(user._id);
                    if (user._id == currentItem._assignedUser) {
                        assignedUserTd.innerHTML = user._username;
                    }
                }

                const modifierTd = document.createElement('TD');
                modifierTd.innerHTML = " ";

                const editTd = document.createElement('TD');
                const editButton = document.createElement('BUTTON');
                editButton.innerHTML = 'EDIT';
                editButton.addEventListener('click', () => listEditButton_Click(currentItem._id));
                editTd.appendChild(editButton);

                const deleteTd = document.createElement('TD');
                const deleteButton = document.createElement('BUTTON');
                deleteButton.innerHTML = 'DELETE';
                deleteButton.addEventListener('click', () => listDeleteButton_Click(currentItem._id));
                deleteTd.appendChild(deleteButton);

                tr.appendChild(idTd);
                tr.appendChild(titleTd);
                tr.appendChild(createdTd);
                tr.appendChild(updatedTd);
                tr.appendChild(assignedUserTd);
                tr.appendChild(modifierTd);
                tr.appendChild(editTd);
                tr.appendChild(deleteTd);

                listTable.appendChild(tr);
                //console.log(currentItem._id)



            }



        }



    }

}


async function listEditLink_Click() {
    await render(listEditPage());
}

async function listEditButton_Click(id) {

    await listEditLink_Click();
    const item = await ListRepository.getById(id);

    document.getElementById('id').value = item._id;
    document.getElementById('title').value = item._title;
    document.getElementById('assignedUser').value = item._assignedUser;

}

async function listEditForm_Submit() {
    const id = document.getElementById('id').value;
    const title = document.getElementById('title').value;
    const assignUser = document.getElementById("assignedUser").value;

    const item = new List(title);
    item._assignedUser = assignUser;


    if (id == "") {
        await ListRepository.addList(item);
    } else {
        await ListRepository.editList(id, item);

    }

    await todosLink_Click();
}

async function listDeleteButton_Click(id) {

    await ListRepository.deleteList(id);
    await todosLink_Click();
}
