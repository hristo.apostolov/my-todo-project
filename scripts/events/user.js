
async function usersLink_Click() {

    await render(usersPage());
    const usersTable = document.getElementById('usersTable');

    const items = await UsersRepository.getAll();
    if (items == null)
        return;

    for (let i = 0; i < items.length; i++) {
        const currentItem = items[i];

        const tr = document.createElement('TR');

        const usernameTd = document.createElement('TD');
        usernameTd.innerHTML = currentItem._username;

        const passwordTd = document.createElement('TD');
        passwordTd.innerHTML = currentItem._password;

        const firstNameTd = document.createElement('TD');
        firstNameTd.innerHTML = currentItem._firstName;

        const lastNameTd = document.createElement('TD');
        lastNameTd.innerHTML = currentItem._lastName;

        const isAdminTd = document.createElement('TD');
        isAdminTd.innerHTML = currentItem._isAdmin;

        const createdTd = document.createElement('TD');
        createdTd.innerHTML = currentItem._created;

        const updatedTd = document.createElement('TD');
        updatedTd.innerHTML = currentItem._updated;

        const editTd = document.createElement('TD');
        const editButton = document.createElement('BUTTON');
        editButton.innerHTML = 'EDIT';
        editButton.addEventListener('click', () => usersEditButton_Click(currentItem._id));
        editTd.appendChild(editButton);

        const deleteTd = document.createElement('TD');
        const deleteButton = document.createElement('BUTTON');
        deleteButton.innerHTML = 'DELETE';
        deleteButton.addEventListener('click', () => usersDeleteButton_Click(currentItem._id));
        deleteTd.appendChild(deleteButton);

        tr.appendChild(usernameTd);
        tr.appendChild(passwordTd);
        tr.appendChild(firstNameTd);
        tr.appendChild(lastNameTd);
        tr.appendChild(isAdminTd);
        tr.appendChild(createdTd);
        tr.appendChild(updatedTd);
        tr.appendChild(editTd);
        tr.appendChild(deleteTd);

        usersTable.appendChild(tr);
        
    }  
}


async function usersEditLink_Click() {
    await render(usersEditPage());
}

async function usersEditButton_Click(id) {

    await usersEditLink_Click();
    const item = await UsersRepository.getById(id);

    document.getElementById('id').value = item._id;
    document.getElementById('username').value = item._username;
    document.getElementById('password').value = item._password;
    document.getElementById('firstName').value = item._firstName;
    document.getElementById('lastName').value = item._lastName;
    document.getElementById('isAdmin').checked = item._isAdmin;
    
}



async function usersEditForm_Submit() {
    const id = document.getElementById('id').value;
    const username = document.getElementById('username').value;
    const password = document.getElementById('password').value;
    const firstName = document.getElementById('firstName').value;
    const lastName = document.getElementById('lastName').value;
    const isAdmin = (document.getElementById('isAdmin').checked ? true : false);

    const item = new User(username, password, firstName, lastName, isAdmin);
 

    if (id == "") {
        await UsersRepository.addUser(item);
    } else {
        await UsersRepository.editUser(id, item);
       
    }

    await usersLink_Click();
}

async function usersDeleteButton_Click(id) {

    await UsersRepository.deleteUser(id);
    await usersLink_Click();
}


async function loginLink_Click() {

    await render(loginPage());
}

async function loginForm_Submit() {

    const username = document.getElementById('username').value;
    const password = document.getElementById('password').value;

    await AuthenticationService.authenticate(username, password);
    const loggedUser = await AuthenticationService.getLoggedUser();

    if (loggedUser != null) {
        render(homePage());
        handleMenu();
        location.reload();
    } else {
        document.getElementById('error').innerHTML = "User doesn't exist";
    }
}

async function logoutLink_Click() {

    await AuthenticationService.logout();
    handleMenu();
    render(homePage());
}