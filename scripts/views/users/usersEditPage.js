function usersEditPage() {
    return `<input type="hidden" id="id" name="id" />
    <fieldset>
        <legend>User</legend>
        <table class="container">
            <tr>
                <td>Username:</td>
                <td><input type="text" id="username" name="username" /></td>
            </tr>
            <tr>
                <td>Password:</td>
                <td><input type="password" id="password" name="password" /></td>
            </tr>
            <tr>
                <td>First Name:</td>
                <td><input type="text" id="firstName" name="firstName" /></td>
            </tr>
            <tr>
                <td>Last Name:</td>
                <td><input type="text" id="lastName" name="lastName" /></td>
            </tr>
            <tr>
                <td>Is admin:</td>
                <td><input type="checkbox" id="isAdmin" name="isAdmin" /></td>
            </tr>
            
            <tr>
                <td colspan="2"><input type="button" onclick="usersEditForm_Submit()" value="Save" /></td>
            </tr>
        </table>
    </fieldset>`;
}