class User {
    constructor(username, password, firstName, lastName, isAdmin) {
        this._username = username;
        this._password = password;
        this._firstName = firstName;
        this._lastName = lastName;
        this._isAdmin = isAdmin;
        this._created = new Date();
        this._updated = new Date();
    }

    get id() {
        return this._id;
    }

    set id(x) {
        this._id = x;

    }
    get username() {
        return this._username;
    }

    set username(x) {
        this._username = x;
    }

    get password() {
        return this._password;
    }

    set password(x) {
        this._password = x;
    }

    get firstName() {
        return this._firstName;
    }

    set firstName(x) {
        this._firstName = x;
    }

    get lastName() {
        return this._lastName;
    }

    set lastName(x) {
        this._lastName = x;
    }

    get isAdmin() {
        return this._isAdmin;
    }

    set isAdmin(x) {
        this._isAdmin = x;
    }
    get created() {
        return this._created;
    }

    set created(x) {
        this._created= x;
    }

    get updated() {
        return this._updated;
    }

    set updated(x) {
        this._updated= x;
    }


}

