class List {
    constructor(title) {
        this._id=id;
        this._title = title;
        this._created= new Date();
        this._updated = new Date();
        this._assignedUser= assignedUser;
    }

    get id() {
        return this._id;
    }

    set id(x) {
        this._id = x;
      
    }

    get title() {
        return this._title;
    }

    set title(x) {
        this._title = x;
    }

    get created() {
        return this._created.toLocaleString;
    }

    set created(x) {
        this._created.toLocaleString = x;
     
    }
    get updated() {
        return this._updated.toLocaleString;
    }

    set updated(x) {
        this._updated = x;
    }
    get assignedUser() {
        return this._assignedUser;
    }

    set assignedUser(x) {
        this._assignedUser = x;
    }
}

